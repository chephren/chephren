import { LinkState } from './enums/linkState.enum';
import type { NodeSprite } from './nodeSprite.model';

export class Link {
  constructor(
    public readonly target: NodeSprite,
    public state: LinkState | LinkError = LinkState.NOT_STARTED,
  ) {}

  public get hasError(): boolean {
    return this.state['statusCode'] != null;
  }

  public confirmPrevision(): void {
    if (this.state === LinkState.PROVISIONAL) this.state = LinkState.NOT_STARTED;
  }
}
