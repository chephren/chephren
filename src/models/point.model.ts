import { Vector } from './vector.model';

export class Point {
  public constructor(
    public x: number,
    public y: number,
  ) {}

  public plus(vector: Vector): Point {
    return new Point(this.x + vector.x, this.y + vector.y);
  }

  public vectorTo(destination: Point): Vector {
    return new Vector(destination.x - this.x, destination.y - this.y);
  }
}
