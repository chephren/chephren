import { State } from '@/models/enums/state.enum';
import { Node } from '@/models/node.model';
import { NodeUrl } from '@/models/nodeUrl.model';
import { ToastService } from '@/services/toast.service';
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import { useNodesUrlStore } from './nodesUrl.store';

export const useNodesStore = defineStore('nodes', () => {
  const nodeUrlStore = useNodesUrlStore();
  const nodesMap = ref<Map<string, Node>>();
  const lastUpdate = ref<Date>();

  const nodeUrlSuffix = '/api/node';
  const startNodeSuffix = '/api/start';
  const stopNodeSuffix = '/api/stop';

  async function fetchNode(nodeUrl: NodeUrl): Promise<Node> {
    try {
      const response = await fetch(`${nodeUrl.url}${nodeUrlSuffix}`, {
        method: 'GET',
      });

      if (!response.ok) {
        if (response.status === 403) {
          return new Node({
            name: nodeUrl.name,
            address: new URL(nodeUrl.url),
            state: State.OFFLINE,
          });
        }
        return new Node({
          name: nodeUrl.name,
          address: new URL(nodeUrl.url),
          state: State.UNKNOWN,
        });
      }

      const json = await response.json();
      json.name = nodeUrl.name;
      json.address = new URL(nodeUrl.url);
      return new Node(json);
    } catch (e) {
      return new Node({
        name: nodeUrl.name,
        address: new URL(nodeUrl.url),
        state: State.OFFLINE,
      });
    }
  }

  async function fetchNodes(force: boolean = false): Promise<void> {
    if (force) {
      nodesMap.value = undefined;
    }

    if (nodesMap.value) return;

    const urls = nodeUrlStore.getNodeUrls();
    if (!urls || urls.length === 0) {
      return;
    }

    nodesMap.value = new Map();
    urls.forEach((url) => {
      const node = new Node(
        {
          name: url.name,
          address: new URL(url.url),
          state: State.UNKNOWN,
        },
        true,
      );
      nodesMap.value?.set(node.id, node);
      fetchNode(url)
        .then((newNode) => {
          nodesMap.value?.set(node.id, newNode);
        })
        .catch();
    });
    lastUpdate.value = new Date();
  }

  function clearNodes(): void {
    nodesMap.value = undefined;
  }

  async function startNode(nodeId: string): Promise<void> {
    const node = nodesMap.value?.get(nodeId);
    if (!node) return;

    node.state = State.STARTING;

    try {
      const res = await fetch(`${node.address}${startNodeSuffix}`, {
        method: 'PUT',
      });

      if (res.ok) {
        const nodeUrl = new NodeUrl(node.address, node.name);
        const newNode = await fetchNode(nodeUrl);
        node.state = newNode.state;
        node.resourcesCount = newNode.resourcesCount;
        nodesMap.value?.set(nodeId, node);
      } else {
        throw new Error(res.statusText);
      }
    } catch (error: any) {
      console.error(error);
      ToastService.addError('Error starting node', error.message);
      node.state = State.OFFLINE;
    }
  }

  async function stopNode(nodeId: string): Promise<void> {
    const node = nodesMap.value?.get(nodeId);
    if (!node) return;
    node.state = State.STOPPING;

    try {
      const res = await fetch(`${node.address}${stopNodeSuffix}`, {
        method: 'PUT',
      });

      if (res.ok) {
        node.state = State.OFFLINE;
        nodesMap.value?.set(nodeId, node);
      } else {
        throw new Error(res.statusText);
      }
    } catch (error: any) {
      console.error(error);
      ToastService.addError('Error stopping node', error.message);
      node.state = State.ONLINE;
    }
  }

  function changeNodeState(nodeAddress: URL, state: State): void {
    if (!nodesMap.value) return;

    const node = Array.from(nodesMap.value.values()).find((node) => {
      return node.address.host === nodeAddress.host;
    });
    if (!node) return;

    node.state = state;
    nodesMap.value?.set(node.id, node);
  }

  const nodes = computed(() => {
    if (!nodesMap.value) return undefined;

    return Array.from(nodesMap.value.values());
  });

  const nodesName = computed(() => {
    if (!nodes.value) return [];

    return nodes.value.map((node) => ({ label: node.name ?? 'Unknown name', value: node.id }));
  });

  const filteredNodes = computed(() => {
    return (filters: string[] | undefined) => {
      if (!nodes.value) return [];
      if (!filters || filters.length < 1) return nodes.value;

      return nodes.value.filter((node) => {
        return filters.some(
          (filter) =>
            node.name
              ?.toLowerCase()
              .normalize('NFD')
              .includes(filter.trim().toLowerCase().normalize('NFD')) ||
            node.state
              ?.toLowerCase()
              .normalize('NFD')
              .includes(filter.trim().toLowerCase().normalize('NFD')) ||
            node.address?.host
              .toLowerCase()
              .normalize('NFD')
              .includes(filter.trim().toLowerCase().normalize('NFD')),
        );
      });
    };
  });

  const getNodeById = computed(() => {
    return (id: string) => {
      if (!nodes.value) return undefined;

      return nodes.value.find((node) => node.id === id);
    };
  });

  return {
    // state
    lastUpdate,

    // actions
    fetchNodes,
    clearNodes,
    startNode,
    stopNode,
    changeNodeState,

    // getters
    nodes,
    nodesName,
    getNodeById,
    filteredNodes,
  };
});
