export class Angle {
  private constructor(public readonly inRadiants: number) {}

  public static fromDegrees(angleInDegrees: number): Angle {
    return new Angle((Math.PI * angleInDegrees) / 180);
  }

  public static fromRadiants(angleInRadiants: number): Angle {
    return new Angle(angleInRadiants);
  }

  public get cos(): number {
    return Math.cos(this.inRadiants);
  }

  public get inDegrees(): number {
    return (this.inRadiants * 180) / Math.PI;
  }

  public get sin(): number {
    return Math.sin(this.inRadiants);
  }
}
