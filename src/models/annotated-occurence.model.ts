import { Command } from './comand.model';
import type { Occurence } from './occurence.type';

export type AnnotatedCommand = {
  index: number;
  command: Command;
  state: 'equal' | 'conflict' | 'new' | 'missing' | 'changed';
};

export class AnnotatedOccurence {
  name: string = '';
  commands: AnnotatedCommand[] = [];

  static fromReference(reference: AnnotatedOccurence, occurence: Occurence): AnnotatedOccurence {
    const refCommands = reference.commands;

    const cL = occurence.resource?.commands?.length ?? 0;
    const rL = refCommands.length;
    const resCommands: AnnotatedCommand[] =
      occurence.resource?.commands?.map((command, index) => ({
        index: cL - index,
        command,
        state: 'equal',
      })) ?? [];

    for (const resCommand of resCommands) {
      const refCommand = refCommands.find((c) => c.command.id === resCommand.command.id);
      if (!refCommand && cL === rL) {
        resCommand.state = 'changed';
        continue;
      } else if (!refCommand) {
        resCommand.state = 'new';
        continue;
      }
      if (resCommand.index !== refCommand.index) {
        resCommand.state = 'conflict';
        resCommand.index = refCommand.index;
      }
    }

    if (resCommands.length < refCommands.length) {
      const missingCommands = refCommands.filter(
        (refCommand) =>
          !resCommands.find((resCommand) => resCommand.command.id === refCommand.command.id),
      );

      for (const missingCommand of missingCommands) {
        resCommands.push({
          index: missingCommand.index,
          command: new Command({ ...missingCommand, command: '' }),
          state: 'missing',
        });
      }
      resCommands.sort((a, b) => b.index - a.index);
    }

    const res = new AnnotatedOccurence();
    res.name = occurence.node.name;
    res.commands = resCommands;
    return res;
  }
}
