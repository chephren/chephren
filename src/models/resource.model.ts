import { Command } from './comand.model';

export class Resource {
  public id!: string;
  public name?: string;
  public lastUpdate?: Date;
  public commands?: Command[];
  public commandsCount?: number;
  public deleted?: boolean;

  constructor(json: Partial<Resource>) {
    Object.assign(this, json);
  }
}

export type NewResource = {
  name?: string;
  command?: string;
  addresses?: URL[];
};
