import type { Toast } from '@/models/toast.type';

export class ToastService {
  static TOAST_TIMEOUT: number = 5000;
  static EVENT_NAME: string = 'toast';
  static TOAST_ID: number = 0;

  static addToast(toast: Toast, options?: { timeout?: number }) {
    const event = new CustomEvent(ToastService.EVENT_NAME, {
      detail: {
        ...toast,
        timeout: options?.timeout ?? ToastService.TOAST_TIMEOUT,
      },
    });
    document.dispatchEvent(event);
  }

  static addError(label: string, sublabel?: string, options?: { timeout?: number }) {
    ToastService.addToast(
      {
        id: ToastService.TOAST_ID++,
        label,
        sublabel: sublabel ?? '',
        type: 'error',
      },
      { timeout: 0, ...options },
    );
  }

  static addSuccess(label: string, sublabel?: string, options?: { timeout?: number }) {
    ToastService.addToast(
      {
        id: ToastService.TOAST_ID++,
        label,
        sublabel: sublabel ?? '',
        type: 'success',
      },
      options,
    );
  }

  static addWarning(label: string, sublabel?: string, options?: { timeout?: number }) {
    ToastService.addToast(
      {
        id: ToastService.TOAST_ID++,
        label,
        sublabel: sublabel ?? '',
        type: 'warning',
      },
      options,
    );
  }

  static addDev(label: string, sublabel?: string, options?: { timeout?: number }) {
    ToastService.addToast(
      {
        id: ToastService.TOAST_ID++,
        label,
        sublabel: sublabel ?? '',
        type: 'dev',
      },
      { ...options },
    );
  }
}
