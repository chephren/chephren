import { Angle } from './angle.model';

export class Vector {
  public constructor(
    public readonly x: number,
    public readonly y: number,
  ) {}

  public get angle(): Angle {
    return Angle.fromRadiants(Math.atan2(this.y, this.x));
  }

  public get length(): number {
    return Math.sqrt(this.x ** 2 + this.y ** 2);
  }

  public get unit(): Vector {
    return this.times(1 / this.length);
  }

  public times(factor: number): Vector {
    return new Vector(this.x * factor, this.y * factor);
  }
}
