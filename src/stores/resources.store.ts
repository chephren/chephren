import { Command, type NewCommand } from '@/models/comand.model';
import type { Node } from '@/models/node.model';
import type { Occurence } from '@/models/occurence.type';
import { Resource, type NewResource } from '@/models/resource.model';
import { ToastService } from '@/services/toast.service';
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import { useNodesStore } from './nodes.store';

export const useResourcesStore = defineStore('resources', () => {
  const resourcesUrlSuffix = '/api/resources';
  const resourceUrlSuffix = '/api/resource';

  const nodeStore = useNodesStore();

  const updateUrl = (resourceId: string) => `/api/resource/${resourceId}`;
  const presenceUrl = (resourceId: string) => `/api/resources/${resourceId}/presences`;

  /**
   * Map of resources by node id
   */
  const resourcesMap = ref(new Map<string, Resource[]>());
  const occurencesMap = ref(new Map<string, Occurence[]>());

  async function fetchResourceForOccurence(
    node: Node,
    resourceId: string,
    force: boolean = false,
  ): Promise<Occurence> {
    if (force) {
      resourcesMap.value.delete(node.id);
    }

    if (resourcesMap.value.has(node.id)) {
      const resource = resourcesMap.value.get(node.id)?.find((r) => r.id === resourceId);
      if (resource && resource.commands && resource.commands.length > 0) {
        return { node, resource, checked: true };
      }
    }

    try {
      const res = await fetch(`${node.address}${updateUrl(resourceId)}`, {
        method: 'GET',
      });
      if (!res.ok) {
        throw new Error('no');
      }

      const json = await res.json();
      const resource = new Resource(json);

      const _node = resourcesMap.value.get(node.id);
      const oldResource = _node?.find((r) => r.id === resourceId);
      if (oldResource) {
        oldResource.commands = resource.commands;
        oldResource.commandsCount = resource.commandsCount;
      } else {
        _node?.push(resource);
      }

      return { node, resource, checked: true };
    } catch (e: any) {
      return { node, resource: null, checked: true };
    }
  }

  async function fetchResourceOccurencesById(
    nodeId: string,
    resourceId: string,
    force: boolean = false,
  ): Promise<void> {
    if (force) {
      occurencesMap.value.delete(resourceId);
    }

    const occ: Occurence[] = [];
    const url = presenceUrl(resourceId);
    const node = nodeStore.getNodeById(nodeId);
    if (!node) return;
    try {
      const response = await fetch(`${node.address}${url}`, {
        method: 'GET',
      });

      if (!response.ok) {
        ToastService.addError(
          `Failed to fetch resource presence from ${node.name}`,
          response.statusText,
        );
        console.error(`Failed to fetch resource presence from ${node.name}`, response);
      }

      const nodes = nodeStore.nodes;
      if (!nodes) return;

      const json: string[] = await response.json();
      const addressses = json.map((a) => new URL(a));
      const promises: Promise<Occurence>[] = [];
      for (const address of addressses) {
        const node = nodes.find((n) => n.address.host === address.host);
        if (node) {
          promises.push(fetchResourceForOccurence(node, resourceId, force));
        }
      }

      const results = await Promise.allSettled(promises);
      for (const result of results) {
        if (result.status === 'fulfilled') {
          occ.push(result.value);
        }
      }

      occurencesMap.value.set(resourceId, occ);
    } catch (e: any) {
      ToastService.addError(`Failed to fetch resource presence from ${node.name}`, e.message);
      console.error(`Failed to fetch resource presence from ${node.name}`, e);
    }

    occurencesMap.value.set(resourceId, occ);
  }

  async function fetchResourceOccurences(
    node: Node,
    resource: Resource,
    force: boolean = false,
  ): Promise<void> {
    return fetchResourceOccurencesById(node.id, resource.id, force);
  }

  async function fetchResourcesForNode(node: Node, force: boolean = false): Promise<void> {
    if (force) {
      clearResources();
    }

    if (resourcesMap.value.has(node.id)) return;

    try {
      const response = await fetch(`${node.address}${resourcesUrlSuffix}`, {
        method: 'GET',
      });

      if (!response.ok) {
        console.error(`Failed to fetch resources from ${node.name}`, response);
        return;
      }

      const json = await response.json();
      resourcesMap.value.set(
        node.id,
        json.map((resource: any) => new Resource(resource)),
      );
    } catch (e: any) {
      console.error(`Failed to fetch resources from ${node.name}`, e);
      return;
    }
  }

  async function deleteResource(
    node: Node,
    resourceId: string,
    otherNodes: URL[] = [],
    fetchNode: boolean = false,
  ) {
    if (!resourcesMap.value.has(node.id)) return;

    try {
      const response = await fetch(`${node.address}${resourceUrlSuffix}/${resourceId}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          addresses: otherNodes,
        }),
      });

      if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message);
      }

      const resource = resourcesMap.value.get(node.id)?.find((r) => r.id === resourceId);
      if (resource) {
        resource.deleted = true;
        resource.commands?.push(
          new Command({
            command: 'DELETE',
            date: new Date(),
            id: '0',
          }),
        );
      }

      if (fetchNode) {
        await fetchResourcesForNode(node, true);
      }
    } catch (error: any) {
      console.error(error);
      ToastService.addError('Error deleting resource', error.message);
    }
  }

  async function updateResource(node: Node, resource: Resource, newCommand: NewCommand) {
    const url = updateUrl(resource.id);

    try {
      const response = await fetch(`${node.address}${url}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          command: newCommand.command,
          addresses: newCommand.addresses ?? [],
        }),
      });

      if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message);
      }

      const res = await response.json();
      const resourceToUpdate = getResourceById(node.id, resource.id);
      if (!resourceToUpdate) {
        throw new Error('Resource not found');
      }
      resourceToUpdate.commandsCount ??= 1;
      resourceToUpdate.commandsCount += 1;
      resourceToUpdate.commands = res.commands.map((c: any) => new Command(c));
    } catch (error: any) {
      console.error(error);
      ToastService.addError('Error updating resource', error.message);
      throw error;
    }
  }

  async function createResource(node: Node, newResource: NewResource) {
    if (!newResource.name || !newResource.command) {
      throw new Error('Name and command are required');
    }
    newResource.addresses ??= [];

    try {
      const response = await fetch(`${node.address}${resourcesUrlSuffix}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newResource),
      });

      if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message);
      }

      node.resourcesCount ??= 0;
      node.resourcesCount += 1;

      resourcesMap.value.get(node.id)?.push(new Resource(await response.json()));
    } catch (error: any) {
      ToastService.addError('Error creating resource', error.message);
      console.error(error);
      throw error;
    }
  }

  function clearResources() {
    occurencesMap.value.clear();
    resourcesMap.value.clear();
  }

  const occurences = computed(() => {
    return (resourceId: string) => occurencesMap.value.get(resourceId);
  });

  const resources = computed(() => {
    return (nodeId: string) => resourcesMap.value.get(nodeId);
  });

  function getResourceById(nodeId: string, resourceId: string) {
    return resourcesMap.value.get(nodeId)?.find((r) => r.id === resourceId);
  }

  return {
    occurences,
    resources,

    fetchResourcesForNode,
    fetchResourceOccurences,
    fetchResourceOccurencesById,
    clearResources,
    deleteResource,
    updateResource,
    createResource,
  };
});
