import type { Node } from './node.model';
import type { Point } from './point.type';
import { State } from './enums/state.enum';
import { SpriteState } from './enums/spriteState.enum';

export class NodeSprite {
  public centre!: Point;
  public node!: Node;
  public radius!: number;
  public state!: SpriteState;

  public static minGap = 10;

  constructor(json: Partial<NodeSprite>) {
    Object.assign(this, json);
  }

  get name(): string {
    return this.node.name;
  }

  get id(): string {
    return this.node.id;
  }

  get isOffline(): boolean {
    return this.node.state === State.OFFLINE;
  }

  get isOnline(): boolean {
    return this.node.state === State.ONLINE;
  }

  public minDistanceWith(otherNode: NodeSprite): number {
    return this.radius + otherNode.radius + NodeSprite.minGap;
  }
}
