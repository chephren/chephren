import type { State } from '@/models/enums/state.enum';
import type { Node } from '@/models/node.model';
import { useNodesStore } from '@/stores/nodes.store';

export class EventSourceService {
  private static _instance: EventSourceService;
  static get instance(): EventSourceService {
    if (!EventSourceService._instance) {
      EventSourceService._instance = new EventSourceService();
    }
    return EventSourceService._instance;
  }

  constructor() {
    if (EventSourceService._instance) {
      throw new Error(
        'EventSourceService is a singleton class. Use "EventSourceService.instance" instead.',
      );
    }
  }

  private eventSources: Map<string, EventSource> = new Map();
  private tryCount: Map<string, number> = new Map();

  private store = useNodesStore();

  addEventSource(
    node: Node,
    listeners?: {
      onText?: (event: MessageEvent) => void;
      onStatusChange?: (event: MessageEvent) => void;
      onError?: (event: EventSource, node: Node) => void;
    },
  ) {
    this.tryCount.set(node.id, 0);
    const eventSource = new EventSource(`${node.address}/api/subscribe`);
    this.eventSources.set(node.id, eventSource);

    eventSource.addEventListener('close', this.removeEventSource.bind(this, node));
    eventSource.addEventListener('open', this.onOpen.bind(this, node));
    eventSource.addEventListener('error', () => {
      this.onError(eventSource, node);
      if (listeners?.onError != null) listeners.onError(eventSource, node);
    });

    eventSource.addEventListener('text', listeners?.onText ?? this.onText.bind(this));
    eventSource.addEventListener(
      'status_change',
      listeners?.onStatusChange ?? this.onStateChange.bind(this),
    );
  }

  removeEventSource(node: Node) {
    const eventSource = this.eventSources.get(node.id);
    if (eventSource) {
      eventSource.close();
      this.eventSources.delete(node.id);
    }
  }

  closeAll() {
    this.eventSources.forEach((eventSource) => {
      eventSource.close();
    });
    this.eventSources.clear();
  }

  onOpen(node: Node) {
    console.info('EventSource opened', node);
  }

  onError(eventSource: EventSource, node: Node) {
    if (eventSource.readyState === EventSource.CONNECTING) {
      this.tryCount.set(node.id, (this.tryCount.get(node.id) ?? 0) + 1);
      if (this.tryCount.get(node.id)! > 3) {
        this.removeEventSource(node);
        return true;
      }
    }
    return false;
  }

  onStateChange(event: Event) {
    const data: { node_address: string; status: State } = JSON.parse((event as MessageEvent).data);
    const url = URL.canParse(data.node_address)
      ? new URL(data.node_address)
      : new URL(`${import.meta.env.VITE_DEFAULT_PROTOCOL}://${data.node_address}`);
    this.store.changeNodeState(url, data.status);
  }

  onText(event: MessageEvent) {
    console.info(event);
  }
}
