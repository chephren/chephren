<script setup lang="ts">
import NodeArrowsGraph from '@/components/map/NodeArrowsGraph.vue';
import NodeComponent from '@/components/map/NodeComponent.vue';
import MapOverlay from '@/components/map/MapOverlay.vue';
import SpriteResearch from '@/components/map/SpriteResearch.vue';
import { storeToRefs } from 'pinia';
import { onMounted, ref, watch } from 'vue';

import { Point } from '@/models/point.model';
import type { Resource } from '@/models/resource.model';
import { Link } from '@/models/link.model';
import { LinkState } from '@/models/enums/linkState.enum';
import type { Node } from '@/models/node.model';
import { NodeSprite } from '@/models/nodeSprite.model';
import { State } from '@/models/enums/state.enum';
import { SpriteState } from '@/models/enums/spriteState.enum';

import { useNodesStore } from '@/stores/nodes.store';
import { useResourcesStore } from '@/stores/resources.store';

import { EventSourceService } from '@/services/eventSource.service';

const nodeSprites = ref<readonly NodeSprite[]>([]);

const nodesStore = useNodesStore();
const resourcesStore = useResourcesStore();

const eventSourceService = EventSourceService.instance;

const originSprite = ref<NodeSprite>(null);
const links = ref<readonly Link[]>([]);

const collisionEnabled = ref(true);

const svgFrame = ref<SVGElement>();

const dragNode = (spriteToDrag: NodeSprite, offset: Point) => {
  nodeSprites.value = [
    ...nodeSprites.value.filter((node: NodeSprite) => node.id !== spriteToDrag.id),
    spriteToDrag,
  ];

  const coincidingSprites = [];
  const notCoincidingSprites = [];

  nodeSprites.value.forEach((sprite) => {
    if (sprite.id === spriteToDrag.id) return;

    const vector = spriteToDrag.centre.vectorTo(sprite.centre);

    if (vector.length < sprite.minDistanceWith(spriteToDrag)) coincidingSprites.push(sprite);
    else notCoincidingSprites.push(sprite);
  });

  document.onmousemove = (event: MouseEvent) => {
    spriteToDrag.centre = new Point(
      event.clientX + offset.x,
      event.clientY - svgFrame.value?.getBoundingClientRect().y + offset.y,
    );

    if (!collisionEnabled.value) return;

    notCoincidingSprites.forEach((sprite: NodeSprite) => {
      const vector = spriteToDrag.centre.vectorTo(sprite.centre);
      const minDistance = sprite.minDistanceWith(spriteToDrag);

      if (vector.length < minDistance) {
        sprite.centre.x = Math.max(
          sprite.radius,
          Math.min(
            spriteToDrag.centre.x + vector.angle.cos * minDistance,
            svgFrame.value?.getBoundingClientRect().width ?? 0 - sprite.radius,
          ),
        );
        sprite.centre.y = Math.max(
          sprite.radius,
          Math.min(
            spriteToDrag.centre.y + vector.angle.sin * minDistance,
            svgFrame.value?.getBoundingClientRect().height ?? 0 - sprite.radius,
          ),
        );
      }
    });

    coincidingSprites.forEach((sprite: NodeSprite, i: number) => {
      const vector = spriteToDrag.centre.vectorTo(sprite.centre);

      if (vector.length >= sprite.minDistanceWith(spriteToDrag)) {
        coincidingSprites.splice(i, 1);
        notCoincidingSprites.push(sprite);
      }
    });
  };
};

const onNodeDrag = (spriteToDrag: NodeSprite, mousePosition: Point) => {
  const offset = mousePosition.vectorTo(spriteToDrag.centre);
  dragNode(spriteToDrag, offset);
};

const onCollisionToggled = (newValue: boolean) => {
  collisionEnabled.value = newValue;
};

const onResearchedNodeDrag = (spriteToDrag: NodeSprite, offset: Point) => {
  dragNode(spriteToDrag, offset);
};

const undragNode = () => {
  document.onmousemove = null;
};

document.addEventListener('mouseup', undragNode);

const canEditResourcesOf = (sprite: NodeSprite) =>
  sprite.isOnline &&
  (sprite.state === SpriteState.IDLE ||
    (sprite.state === SpriteState.PROPAGATING && originSprite.value?.id === sprite.id));

const canHideLinksOn = (sprite: NodeSprite) => sprite.state === SpriteState.SENT;

const canSwitchOnOff = (sprite: NodeSprite) =>
  (sprite.isOnline || sprite.isOffline) && !originSprite.value;

const handleCommandSending = async (
  sendingSprite: NodeSprite,
  resource: Resource,
  command: string,
) => {
  links.value.forEach((link: Link) => link.confirmPrevision());

  eventSourceService.addEventSource(sendingSprite.node, {
    onStatusChange: (event: MessageEvent) => {
      const { message, node_address, node_failed, status, statusCode } = JSON.parse(event.data);

      if (statusCode != null) {
        if (node_failed != null) {
          const link = links.value.find(
            (link) => link.target.node.address.host === new URL(node_failed).host,
          );
          if (link != null) link.state = { message, statusCode };
        } else {
          links.value.forEach((link) => {
            link.state = { message, statusCode };
          });
        }
      } else {
        nodesStore.changeNodeState(new URL(node_address), status);
      }
    },
  });

  links.value.forEach((link) =>
    eventSourceService.addEventSource(link.target.node, {
      onStatusChange: (event: MessageEvent) => {
        const { message, node_address, status, statusCode } = JSON.parse(event.data);
        nodesStore.changeNodeState(new URL(node_address), status);
        link.target.node.state = status;

        if (status === 'UPDATING') link.state = LinkState.PENDING;
        else if (status === 'ONLINE') {
          if (statusCode == null) link.state = LinkState.SUCCESS;
          else link.state = { statusCode, message };

          eventSourceService.removeEventSource(link.target.node);
        }
      },
      onError: () => {
        link.state = { statusCode: 404, message: 'Unreachable' };
      },
    }),
  );

  nodeSprites.value.forEach((node: NodeSprite) => {
    if (node.id === originSprite.value.id) node.state = SpriteState.SENDING;
    else node.state = SpriteState.RECEIVING;
  });

  await resourcesStore.updateResource(sendingSprite.node, resource, {
    command,
    addresses: links.value.map((link) => link.target.node.address),
  });

  eventSourceService.removeEventSource(sendingSprite.node);
  sendingSprite.state = SpriteState.SENT;
};

const handleNewCommand = (notifyingSprite: NodeSprite) => {
  originSprite.value = notifyingSprite;
  nodeSprites.value.forEach((node: NodeSprite) => {
    if (node.id === notifyingSprite.id) node.state = SpriteState.PROPAGATING;
    else node.state = SpriteState.PENDING;
  });
};

const handleResourceSelected = async (notifyingSprite: NodeSprite, resource: Resource) => {
  originSprite.value = notifyingSprite;
  await resourcesStore.fetchResourceOccurencesById(notifyingSprite.id, resource.id);

  links.value = [
    ...links.value,
    ...resourcesStore
      .occurences(resource.id)
      .filter(({ node }) => node.id !== notifyingSprite.id)
      .map(({ node }) => nodeSprites.value.find((sprite) => sprite.id === node.id))
      .map((sprite) => new Link(sprite, LinkState.PROVISIONAL)),
  ];
};

const handleResourceUnselected = () => {
  links.value = links.value.filter((link) => link.state !== LinkState.PROVISIONAL);
};

const handleClick = (notifyingSprite: NodeSprite) => {
  if (notifyingSprite.node.state === State.ONLINE) {
    resourcesStore.fetchResourcesForNode(notifyingSprite.node);
  }

  if (
    originSprite.value?.state === SpriteState.PROPAGATING &&
    notifyingSprite.state === SpriteState.PENDING
  ) {
    if (links.value.some((link) => link.target.id === notifyingSprite.id))
      links.value = links.value.filter((link) => link.target.id !== notifyingSprite.id);
    else links.value = [...links.value, new Link(notifyingSprite)];
  }
};

const hideLinks = () => {
  nodeSprites.value.forEach((sprite) => (sprite.state = SpriteState.IDLE));
  links.value = [];
  originSprite.value = null;
};

watch(storeToRefs(nodesStore).nodes, () => {
  nodeSprites.value = storeToRefs(nodesStore).nodes.value.map(
    (node: Node, i: number) =>
      new NodeSprite({
        centre: new Point((i % 6) * 200 + 75, Math.floor(i / 6) * 200 + 75),
        node,
        radius: 70,
        state: SpriteState.IDLE,
      }),
  );
});

onMounted(nodesStore.fetchNodes);
</script>

<template>
  <svg width="100%" height="100%" ref="svgFrame">
    <NodeArrowsGraph :origin="originSprite" :links="links" />
    <g>
      <NodeComponent
        v-for="sprite in nodeSprites"
        :key="sprite.id"
        :sprite="sprite"
        :canEditResources="canEditResourcesOf(sprite)"
        :canHideLinks="canHideLinksOn(sprite)"
        :canSwitchOnOff="canSwitchOnOff(sprite)"
        @drag="(mousePosition) => onNodeDrag(sprite, mousePosition)"
        @resourceSelected="(resource) => handleResourceSelected(sprite, resource)"
        @linksHidden="hideLinks"
        @resourceUnselected="handleResourceUnselected"
        @newCommand="() => handleNewCommand(sprite)"
        @commandSent="(resource, command) => handleCommandSending(sprite, resource, command)"
        @click="() => handleClick(sprite)"
      />
    </g>
    <MapOverlay
      :collisionEnabled="collisionEnabled"
      :sprites="nodeSprites"
      @collisionToggled="onCollisionToggled"
      @spriteFound="onResearchedNodeDrag"
    />
  </svg>
  <a href="/">
    <img src="/nodes-cards-view.png" />
  </a>
</template>

<style scoped lang="scss">
svg {
  user-select: none;
}

a {
  display: block;

  position: fixed;
  left: 1rem;
  bottom: 1rem;

  height: 6rem;
  width: 6rem;

  padding: 0.2rem;

  border: solid 2px var(--primary);
  border-radius: 10px;

  background-color: white;

  img {
    width: 100%;
    height: 100%;

    object-fit: cover;
  }
}
</style>
