/// <reference types="vite/client" />

interface ImportMetaEnv {
  VITE_DEFAULT_PROTOCOL: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
