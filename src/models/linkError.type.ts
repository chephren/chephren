export type LinkError = {
  statusCode: number;
  message: string;
};
